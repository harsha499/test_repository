
const axios = require('axios');

module.exports.hello1 = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v4.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};


module.exports.check1 = async (event) => {

let test =  await axios.get("https://dummy.restapiexample.com/api/v1/employees");

  return {
    statusCode: 200,
    body: JSON.stringify(test.data)
  };
}